interface GenerateExcelInterface {
  generateFile(): boolean
}

interface RemoveExcelInterface {
  removeFile(): boolean
}


export class GenerateExcel implements GenerateExcelInterface,
  RemoveExcelInterface {

  private fileName: string;
  private destination: string

  constructor(fileName: string, dirPath: string) {

    if (!fileName.trim()) {
      throw Error("error fileName is empty")
    }

    if (!dirPath.trim()) {
      throw Error("error directory Path is empty")
    }
    this.fileName = fileName
    this.destination = dirPath
  }

  generateFile(): boolean {
    return false;
  }

  removeFile(): boolean {
    return false;
  }
}