import { Argv } from "yargs"
import moment from "moment"
import { EventStore, Op } from "../src/models/EventStore";
import * as Excel from 'exceljs'
import { Worksheet } from "exceljs";
import * as EmailValidator from 'email-validator'
import { Client } from 'postmark'
import fs from 'fs'


export const command: string = 'clientLogin'

export const describe: string = 'generate a xlsx file for a login monitoring'

export const builder = (args: Argv): Argv => {

  args
    .option('email_address', {
      alias: "email",
      describe: "attach a generated excel in email",
      default: 'techaz@importgenius.com'
    })
    .option('send', {
      default: "true",
      type: "string",
      describe: "send an email",
      choices: ["true", "false"]
    })
    .option('file_name', {
      default: "All Login Attempts (Failed and Successful) to ISNG",
      describe: "name of the xlxs file "
    })


  return args
}

const sendEmail = async (email, fileName) => {
// @ts-ignore
  const client = new Client(process.env.postmark_api_key);
  const dateNow = moment().format("YYYY-MM-DD");

  const sendEmail = await client.sendEmail({
    From: "techaz@importgenius.com",
    To: email,
    Subject: `All ISNG Logins - ${dateNow}`,
    TextBody: "Hi, ",
    Attachments: [{
      Content: fs.readFileSync(`./generatedReport/${fileName}.xlsx`).toString("base64"),
      Name: `${fileName}.xlsx`,
      ContentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    }]
  })

  return sendEmail;

}

export const handler = async (args: Argv) => {

  console.log(moment().format("MM-DD-YYYY hh:mm:ss a"))
  try {
    const startDate = moment().startOf('day')
      .subtract(1, 'day').unix();

    const endDate = moment().endOf('day')
      .subtract(1, 'day').unix();

    console.log(`startDate: ${startDate} , endDate: ${endDate}`);

    // @ts-ignore
    args.email_address.split(',').map(email => {
      if (!EmailValidator.validate(email.trim()))
        throw Error('Invalid email address')
    })

    // @ts-ignore
    const fileName = args.file_name;

    // @ts-ignore
    const email = args.email_address;

    let workbook = new Excel.Workbook();


    await getEventsFrom(workbook,
      ['ClientSuccessfullyLoggedIn'],
      startDate,
      endDate,
      0,
      'Successful Logins'
    );

    await getEventsFrom(workbook,
      ['ClientFailedToLogin', 'AnonymousLoginAttempted'],
      startDate,
      endDate,
      1,
      'Failed Logins'
    )

    await generateFile(workbook, fileName);

    // @ts-ignore
    if (args.send != 'false')
      await sendEmail(email, fileName);

    process.exit();

  } catch (e) {
    console.log(`throw an error: ${e.message}`)
  }

}

const generateFile = async (workbook,fileName) => {
  await workbook.xlsx.writeFile(`./generatedReport/${fileName}.xlsx`)
}


const getEventsFrom = async (
  workbook,
  eventNames: Array<String>,
  startDate,
  endDate,
  sheetIndex: number,
  excelTitle: string) => {

  const workSheet = workbook.addWorksheet(excelTitle,
    { state:'visible' });

  workbook.getWorksheet(sheetIndex)
  workSheet.columns = [
    { header: "Username", key: "username" },
    { header: "IP Address", key: "ipAddress" },
    { header: "Login Time", key: "loginTime" },
    { header: "Login Date", key: "loginDate" },
    { header: "Result", key: "result" },
  ];

  const eventStore = await EventStore.findAll({
    where: {
      entity_type: 'ClientLogHistory',
      event_name: {
        [Op.in]: eventNames
      },
      timestamp: {
        [Op.between]: [startDate, endDate]
      }
    },
    order: [
      ['timestamp', 'DESC']
    ]
  });

  createExcel(workSheet, eventStore);
};

const createExcel = (workSheet: Worksheet, eventStores): Worksheet => {

  eventStores.forEach( (eventStore) => {
    const data = JSON.parse(eventStore.data)
    const eventName = eventStore.event_name;
    let result:string;

    switch (eventName) {
      case "ClientSuccessfullyLoggedIn":
        result = "Success for successful login";
        break;
      case "ClientFailedToLogin":
        result = "Invalid User for wrong password";
        break
      default:
        result = "Invalid Username for non-existent username"
    }
    
    workSheet.addRow({
      username: data.username,
      ipAddress: data.ipAddress || eventStore.ip_address,
      loginTime: moment.unix(eventStore.timestamp).format('hh:mm:ss a'),
      loginDate: moment.unix(eventStore.timestamp).format('YYYY-MM-DD'),
      result: result

    })
  })

  return workSheet
}