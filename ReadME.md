# ISNG Cli Command
Node.js project for IG's cli command

## REQUIREMENTS
Yarn must be installed globally - https://yarnpkg.com/en/docs/install
Node.js engine must match the specified version in package.json

## INSTALLATION
Run yarn install to install dependencies

## To run the script
to see all the command in the script
```
yarn generate-report --help
```
ex.
1. to see the options of generate client login report
```
yarn generate-report clientLogin --help
```
the result will be like this
```
 --version                         Show version number                [boolean]
  --help                            Show help                          [boolean]
  --email_address, --email          send a generated excel in email
                                            [default: "techaz@importgenius.com"]
  --send                            send an email
                           [string] [choices: "true", "false"] [default: "true"]
  --file_name                       name of the xlxs file to generateFile
                 [default: "All Login Attempts (Failed and Successful) to ISNG"]


```

to run the example script:
```
yarn generate-report clientLogin --file_name="samplefile" --send=false
```
this will generate a file named "samplefile.xlsx" in the generateReport folder