import yargs from 'yargs'

import * as  monitoring from './cmds/clientLoginMonitoring'

yargs.command(monitoring)
  .demandCommand()
  .help()
  .argv

