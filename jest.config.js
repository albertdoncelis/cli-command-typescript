module.exports = {
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  "testResultsProcessor": "jest-stare",
  "reporters": [
    "default"
  ],
  "collectCoverage": true,
  "collectCoverageFrom": [
    "src/**/*.{js,jsx,ts}",
    "lib/**/*/.{js,jsx,ts}",
    "!src/index.js",
    "!dist/**/*.{js,jsx,ts}"
  ],
  "coverageDirectory": "reports/latest/coverage",
  "coverageReporters": ["json", "html"]
};