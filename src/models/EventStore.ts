import * as Sequelize from 'sequelize'
import { sequelize } from "../instances/sequelize"

export const Op = Sequelize.Op;

export const EventStore = sequelize.define('events', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  stream_id: Sequelize.STRING,
  event_id: Sequelize.STRING,
  event_name: Sequelize.STRING,
  entity_type: Sequelize.STRING,
  entity_id: Sequelize.STRING,
  ip_address: Sequelize.STRING,
  timestamp: Sequelize.DATE,
  data: Sequelize.JSON

}, {
  createdAt: false,
  updatedAt: false
})