import Sequelize from 'sequelize'

const database = process.env.event_store_database || 'events';
const username = process.env.event_store_username || 'root';
const password = process.env.event_store_password || 'password';
const host = process.env.event_store_host || 'localhost';
const port = process.env.event_store_port || '3306';

// @ts-ignore
export const sequelize = new Sequelize(database, username, password, {
  host,
  dialect: 'mysql',
  port,
  pool: {
    max: 5,
    min:0,
    acquire: 30000,
    idle: 10000
  },
  operatorsAliases: false,
  logging: false
})