import { GenerateExcel } from '../../lib/generateExcel'
import faker from 'faker'

describe("create xlsx", function() {

  it("should throw an error when the filename is empty", () => {
    const dirPath = faker.system.directoryPath()
    function error() {
      new GenerateExcel(
        "",
        dirPath);
    }
    expect(error).toThrowError()

  })

  it("should throw an error when the filename is empty", () => {
    const fileName = faker.system.fileName()
    function error() {
      new GenerateExcel(
        fileName,
        "");
    }

    expect(error).toThrowError()
  })

});